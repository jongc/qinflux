"""
Test module for qinflux package.

These tests can be run by spinning up an influxdb instance using the following docker command:

    docker run --rm -e INFLUXDB_DB=testingdb -p 8086:8086 influxdb

Note that this starts an insecure server.
"""
import os
import logging
from qinflux import write_measurement, QuickInfluxConnection
from argparse import Namespace

logging.basicConfig(level=logging.DEBUG)

os.environ['INFLUX_DATABASE'] = 'testingdb'
os.environ['INFLUX_USERNAME'] = 'testuser'
os.environ['INFLUX_PASSWORD'] = 'pw1'

# Simplest write - no connection required.

write_measurement(measurement='howbig', fields={'bigness': 100.00}, tags={'color': 'green'})


# It is possible to write to a connection created using the QuickInfluxConnection context manager.
# The connection will be closed after the context manager completes.

with QuickInfluxConnection() as client:
    client.write_measurement(measurement='howbig', fields={'bigness': 150.00}, tags={'color': 'green'})


# The context manager can accept arguments that will be used when creating the InfluxDB connection.
# Any parameters that are valid on the influxdb.InfluxDBClient constructor can be passed.

with QuickInfluxConnection(username='testuser') as client:
    client.write_measurement(measurement='howbig', fields={'bigness': 150.00}, tags={'color': 'green'})


# Command line arguments can also be passed.  In this example we will fake some command line arguments
# by creating an argparse.Namespace.

command_line_args = Namespace(username='fred', password='barney')

with QuickInfluxConnection(command_line_args=command_line_args) as client:
    results = client.query('SELECT "bigness" FROM "testingdb"."autogen"."howbig"')
    print(results.raw)
