#!/usr/bin/env python3
"""
qinflux = Quick InfluxDB.

To test this module you can start a docker container using the following command:

    docker run --rm -e INFLUXDB_DB=testingdb -p 8086:8086 influxdb

This module provides lightening quick implementation of measurement writing to influxdb with minimal boilerplate.
In a minimalist setup connections details are inferred from environment variables, meaning the only code you need to
write in order to write a measurement to influxdb is the :func:`write_measurement()` function call.

A connection context manager is included, which handles inferring connection details, allowing minimalist calls.

Connection arguments can be overridden by directly including arguments, or by passing command line arguments.

Environment variables
---------------------

The package makes use of the following environment variables:

  - INFLUX_HOST
  - INFLUX_PORT
  - INFLUX_DATABASE
  - INFLUX_USERNAME
  - INFLUX_PASSWORD

Installation
------------

    pip3 install qinflux

Examples:

    The examples make use of environment variables to provide connection information.  Lets set those up now:

        >>> import os
        >>> os.environ['INFLUX_DATABASE'] = 'testingdb'
        >>> os.environ['INFLUX_USERNAME'] = 'testuser'
        >>> os.environ['INFLUX_PASSWORD'] = 'pw1'

    This example shows how you can write a measurement in one line of code with no setup:

        >>> write_measurement(measurement='howbig', fields={'bigness': 100.00}, tags={'color': 'green'})
        True

    Using the context manager allows you to communicate with an influxdb instance without any boilerplate:

        >>> with QuickInfluxConnection() as client:
        ...     client.get_list_database()
        [{'name': 'testingdb'}, {'name': '_internal'}]

    You can use the connection manager to write to influxdb if you need to pass connection arguments.  This uses
    a :func:`write_measurement` function that is added by this package.  This is a simplified function that saves
    you having to create your own data payload.

        >>> with QuickInfluxConnection(username='anotheruser') as client:
        ...     client.write_measurement(measurement='howbig', fields={'bigness': 100.00}, tags={'color': 'green'})
        True

"""
import os
import argparse
import influxdb
import logging

#: Default influxdb hostname.
DEFAULT_INFLUX_HOST = 'localhost'

#: Default influxdb port number.
DEFAULT_INFLUX_PORT = '8086'


class Bunch(dict):
    """Bunch object for holding stuff in a dict, while pretending to be an object.

    Example:
        >>> b = Bunch()
        >>> b.foo = 'fred'
        >>> b.foo
        'fred'
        >>> b['foo']
        'fred'

        >>> b = Bunch(foobar='baz')
        >>> b.foobar
        'baz'

        >>> b.bar
        Traceback (most recent call last):
        ...
        KeyError: 'Attribute "bar" not found.'
    """
    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            self[k] = v

    def __getattr__(self, item):
        if item in self:
            return self[item]
        else:
            raise KeyError('Attribute "%s" not found.' % item)

    def __setattr__(self, key, value):
        self[key] = value


class InfluxDBConnectionDetails(Bunch):
    """A class for InfluxDB connection details.

    The InfluxDBConnectionDetails class contains all of the arguments that could be used when connecting to InfluxDB.

    Object instances can be referenced as dictionaries or using attribtues.  A class instance can be used to fill
    arguments in the :func:`influxdb.InfluxDBClient` constructor.  This can be done using the ``**kwargs`` syntax:

    Examples:

        Creating an instance:

            >>> connection_args = InfluxDBConnectionDetails(host='localhost', port=8086, username='user', password='pw',
            ...                                             database='testingdb')

        A connection can be made to InfluxDB by unpacking the arguments into the constructor:

            >>> client = influxdb.InfluxDBClient(**connection_args)

        The connection details can be displayed by printing the object:

            >>> print(connection_args)
            InfluxDBConnectionDetails(host='localhost', port=8086, username='user', password='xxxxx', database='testingdb')

        Connection values can be inferred from command line arguments.  Before we start lets create some environment
        variables:

            >>> import os
            >>> os.environ['INFLUX_USERNAME'] = 'fred'
            >>> os.environ['INFLUX_PASSWORD'] = 'wilma1'
            >>> os.environ['INFLUX_DATABASE'] = 'demo'

        Now lets create an instance, and check the connection details:

            >>> connection_args = InfluxDBConnectionDetails()
            >>> print(connection_args)
            InfluxDBConnectionDetails(host='localhost', port='8086', username='fred', password='xxxxx', database='demo')

        Command line arguments that are set by environment variables can be overridden by including their value
        in the constructor:

            >>> connection_args = InfluxDBConnectionDetails(host='localhost', port=8086, username='user', password='pw',
            ...                                             database=None)
            >>> connection_args
            InfluxDBConnectionDetails(host='localhost', port=8086, username='user', password='xxxxx', database=None)

    """
    def __init__(self, command_line_args=None, **kwargs):
        """Create an instance of InfluxDBConnectionDetails.

        The connection details are taken, in order of precedence, from command line arguments, environment variables or
        default values.

        Args:
            command_line_args(:obj:`argparse.Namespace`, optional): Command line arguments.
            **kwargs: Any arguments that can be passed to the :func:`influxdb.InfluxDBClient` constructor.
        """
        super(InfluxDBConnectionDetails, self).__init__()
        # Create minimal attributes.
        self.host = os.getenv('INFLUX_HOST', DEFAULT_INFLUX_HOST)
        self.port = os.getenv('INFLUX_PORT', DEFAULT_INFLUX_PORT)
        self.username = os.getenv('INFLUX_USERNAME', None)
        self.password = os.getenv('INFLUX_PASSWORD', None)
        self.database = os.getenv('INFLUX_DATABASE', None)

        # Update attributes with any command line arguments passed, but only if they are not None
        if command_line_args:
            for k in ['host', 'port', 'username', 'password', 'database']:
                cli_value = getattr(command_line_args, k, None)
                if cli_value:
                    setattr(self, k, cli_value)

        # Update attributes with any kwargs passed
        if kwargs:
            logging.debug('Updating connection details with constructor arguments.')
            for k, v in kwargs.items():
                setattr(self, k, v)

    def __repr__(self):
        arg_string = ', '.join(['%s=%s' % (k, repr(v) if k != 'password' else repr('xxxxx')) for k, v in self.items()])
        return '%s(%s)' % (self.__class__.__name__, arg_string)


class QuickInfluxClient(influxdb.InfluxDBClient):

    def write_measurement(self, measurement, fields, tags=None):
        """Send a metric to InfluxDB.

        Args:
            measurement(str): Name of measurement.
            fields(dict): Dictionary containing named values.
            tags(dict, optional): Dictionary containing tag names and tag values.

        Returns:
            bool: Result of write operation.
        """
        data = get_data(measurement, fields, tags)
        return self.write_points(data)


class QuickInfluxConnection(object):
    """Context manager for InfluxDB connection."""

    def __init__(self, command_line_args=None, **kwargs):
        """Create an InfluxConnection instance.

        Connection arguments are created using the :obj:`InfluxDBConnectionDetails` class.
        Args:
            command_line_args(:obj:`argparse.Namespace`, optional): Command line arguments to be used when
                defining connection arguments.
            **kwargs: Any arguments that can be passed to the :func:`influxdb.InfluxDBClient` constructor.
        """
        self.connection = None
        self.influx_args = InfluxDBConnectionDetails(command_line_args=command_line_args, **kwargs)
        assert self.influx_args.host, 'Missing hostname.  Please set the hostname as INFLUX_HOST environment ' \
                                      'variable, or pass it in the function calls.'
        assert self.influx_args.port, 'Missing port number.  Please set the port as INFLUX_PORT environment ' \
                                      'variable, or pass it in the function calls.'
        assert self.influx_args.database, 'Missing database.  Please set the database as INFLUX_DATABASE environment ' \
                                          'variable, or pass it in the function calls.'

    def __enter__(self):
        """Returns: QuickInfluxClient"""
        logging.debug('Opening influxdb connection with args: %s', self.influx_args)
        self.connection = QuickInfluxClient(**self.influx_args)

        return self.connection

    def __exit__(self, exc_type, exc_val, exc_tb):
        logging.debug('Closing influxdb connection.')
        self.connection.close()


def get_data(measurement, fields, tags=None):
    """Return a data object that can be written to influx via write_points function.

    Args:
        measurement(str): Name of measurement.
        fields(dict): Dictionary containing named values.
        tags(dict): Dictionary containing tag names and tag values.
    Returns:
        list of dict: A data structure that can be written to influxDB via the :func:`write_points` function."""
    if not tags:
        tags = {}
    json_body = [
        {"measurement": measurement,
         "tags": tags,
         "fields": fields
         },
    ]
    return json_body


def write_measurement(measurement, fields, tags=None, args=None):
    """Send a measurement to InfluxDB.

    Args:
        measurement(str): Name of measurement.
        fields(dict): Dictionary containing named values.
        tags(dict, optional): Dictionary containing tag names and tag values.
        args(argparse.Namespace, optional): Command line arguments for connecting to influxDB.

    Returns:
        bool: Result of write operation.
    """
    with QuickInfluxConnection(command_line_args=args) as conn:
        return conn.write_measurement(measurement, fields, tags)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--host', help='InfluxDB host name.  If no host is passed as a command line argument then'
                                             'the host will be picked up from the INFLUX_HOST environment variable.')
    parser.add_argument('-o', '--port', help='InfluxDB port number.')
    parser.add_argument('-u', '--username', help='InfluxDB username.')
    parser.add_argument('-p', '--password', help='InfluxDB password.')
    parser.add_argument('-d', '--database', help='InfluxDB database.')
    parser.add_argument('-m', '--measurement', required=True, help='Measurement name.')
    parser.add_argument('-f', '--field', required=True, help='Measurement field name.')
    parser.add_argument('-v', '--value', required=True, help='Measurement field value.')
    parser.add_argument('-t', '--tags', help='Tags to be attached to measurement.')
    return parser.parse_args()


def main():
    args = parse_args()
    fields = {args.field: args.value}
    tags = {}
    write_measurement(args.measurement, fields, tags=tags, args=args)


if __name__ == '__main__':
    main()
