# qinflux = Quick Influx
qinflux is a Python package providing a very quick method of sending
information to InfluxDB databases.

Host, post, user and database information is provided via environment
variables.

